#include "main.h"
#include "protibadi-contacts.h"
#include "protibadi-data-processing.h"


static contacts_list_h contacts_list;
int err = CONTACTS_ERROR_NONE;

int contacts_initialization_number(){
	int counter = 0;
if (contacts_connect() == CONTACTS_ERROR_NONE)
{
    if (contacts_db_get_all_records(_contacts_contact._uri, 0, 0, &contacts_list) == CONTACTS_ERROR_NONE)
    {
        dlog_print(DLOG_INFO, LOG_TAG, "Listing contacts...");
        contacts_record_h contact;


        while (contacts_list_get_current_record_p(contacts_list, &contact) == CONTACTS_ERROR_NONE)
        {
            // Contact found.
            bool is_phone_number = false;
            char *contact_name = NULL, *contact_number = NULL;
            err = contacts_record_get_str(contact, _contacts_contact.display_name, &contact_name);
            if (err != CONTACTS_ERROR_NONE)
            {
                dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when trying to get contact's display name! Error code: %d", err);
                contact_name = strdup("Not defined");
            }
            // Notice that if you use the default Contacts app
            // to add a contact without a name and only with phone number,
            // the display name retrieved will be the phone number itself.

            contacts_record_h child_record_phone;
            int child_records_count = 0;

            err = contacts_record_get_child_record_count(contact, _contacts_contact.number, &child_records_count);
            if (err == CONTACTS_ERROR_NONE)
            {
                if (child_records_count == 1) // Only one number found
                {
                    err = contacts_record_get_child_record_at_p(contact, _contacts_contact.number, 0, &child_record_phone);
                    if (err == CONTACTS_ERROR_NONE)
                    {
                        err = contacts_record_get_str(child_record_phone, _contacts_number.number, &contact_number);
                        if (err == CONTACTS_ERROR_NONE)
                        {
                            // Successfully retrieved the phone number
                            // so we set the bool to know that we don't need a placeholder.
                            is_phone_number = true;
                        }
                        else
                        {
                        	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number! Error code: %d", err);
                        }
                    }
                    else
                    {
                    	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number record! Error code: %d", err);
                    }
                }
                else if (child_records_count > 1) // If there is more than one phone number we will display only the default one.
                {
                    int i;
                    for (i = 0; i < child_records_count; i++)
                    {
                        err = contacts_record_get_child_record_at_p(contact, _contacts_contact.number, i, &child_record_phone);
                        if (err == CONTACTS_ERROR_NONE)
                        {
                            bool is_default = false;
                            err = contacts_record_get_bool(child_record_phone, _contacts_number.is_default, &is_default);
                            if (err == CONTACTS_ERROR_NONE)
                            {
                                if (is_default == false)
                                {
                                	dlog_print(DLOG_INFO, LOG_TAG,"Many numbers were defined for this contact and this is not a default one. We ignore this number.");
                                }
                                else
                                {
                                    //Default phone number found
                                    err = contacts_record_get_str(child_record_phone, _contacts_number.number, &contact_number);
                                    if (err == CONTACTS_ERROR_NONE)
                                    {
                                        // Successfully retrieved the phone number
                                        // so we set the bool to know that we don't need a placeholder.
                                        is_phone_number = true;
                                        break; //We have found a default number, we can finish browsing.
                                    }
                                    else
                                    {
                                    	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number! Error code: %d", err);
                                    }
                                }
                            }
                            else
                            {
                            	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when getting the 'is default' attribute! Error code: %d", err);
                            }
                        }
                        else
                        {
                        	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number record! Error code: %d", err);
                        }
                    }
                }
                else
                {
                    // No phone numbers were found for the specified contact.
                    // This can happen for all contacts where no phone number was defined.
                    // You can do something specific to the no-phone contacts, for example filter them.
                    // In this snippet we have already set the bool is_phone_number to false by default,
                    // so we do nothing here.
                }
            }
            else
            {
            	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the count of phone numbers! Error code: %d", err);
            }
            if(is_phone_number == false)
            {
                // we set the placeholder in place of the phone number.
                contact_number = strdup("Not defined");
            }

            // Here you can make use of the retrieved contact name and phone number.
            // In this snippet it's only printed as a log:
            dlog_print(DLOG_INFO, LOG_TAG,"Contact name: %s,\t Phone number: %s", contact_name, contact_number);

            //strcpy(numbers[counter], contact_number);
            //sprintf(numbers[counter],"%s",contact_number);
            //strncpy(numbers[counter], contact_number,sizeof(numbers[counter]));
            strcpy(numbers_t[counter], contact_number);

            counter++;

            free(contact_name);
            free(contact_number);

            contacts_list_next(contacts_list); // proceed to next contact on a list
        }
        dlog_print(DLOG_INFO, LOG_TAG,"Listing contacts completed!");

        contacts_list_destroy(contacts_list, true);
    }
    else
    {
    	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when getting the contact list! Error code: %d", err);
    }

    if (contacts_disconnect() != CONTACTS_ERROR_NONE)
    {
    	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when disconnecting from contacts service!");
    }
	}
	else
	{
		dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when connecting to contacts service! Error code: %d", err);
	}

	return counter;
}

int contacts_initialization_email(){
	int counter = 0;
if (contacts_connect() == CONTACTS_ERROR_NONE)
{
    if (contacts_db_get_all_records(_contacts_contact._uri, 0, 0, &contacts_list) == CONTACTS_ERROR_NONE)
    {
        dlog_print(DLOG_INFO, LOG_TAG, "Listing contacts...");
        contacts_record_h contact;
        while (contacts_list_get_current_record_p(contacts_list, &contact) == CONTACTS_ERROR_NONE)
        {
            // Contact found.
            bool is_phone_number = false;
            char *contact_name = NULL, *email_address = NULL;
            err = contacts_record_get_str(contact, _contacts_contact.display_name, &contact_name);
            if (err != CONTACTS_ERROR_NONE)
            {
                dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when trying to get contact's display name! Error code: %d", err);
                contact_name = strdup("Not defined");
            }
            // Notice that if you use the default Contacts app
            // to add a contact without a name and only with phone number,
            // the display name retrieved will be the phone number itself.

            contacts_record_h child_record_phone;
            int child_records_count = 0;

            err = contacts_record_get_child_record_count(contact, _contacts_contact.email, &child_records_count);
            if (err == CONTACTS_ERROR_NONE)
            {
                if (child_records_count == 1) // Only one number found
                {
                    err = contacts_record_get_child_record_at_p(contact, _contacts_contact.email, 0, &child_record_phone);
                    if (err == CONTACTS_ERROR_NONE)
                    {
                        err = contacts_record_get_str(child_record_phone, _contacts_email.email, &email_address);
                        if (err == CONTACTS_ERROR_NONE)
                        {
                            // Successfully retrieved the phone number
                            // so we set the bool to know that we don't need a placeholder.
                            is_phone_number = true;
                        }
                        else
                        {
                        	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number! Error code: %d", err);
                        }
                    }
                    else
                    {
                    	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number record! Error code: %d", err);
                    }
                }
                else if (child_records_count > 1) // If there is more than one phone number we will display only the default one.
                {
                    int i;
                    for (i = 0; i < child_records_count; i++)
                    {
                        err = contacts_record_get_child_record_at_p(contact, _contacts_contact.number, i, &child_record_phone);
                        if (err == CONTACTS_ERROR_NONE)
                        {
                            bool is_default = false;
                            err = contacts_record_get_bool(child_record_phone, _contacts_number.is_default, &is_default);
                            if (err == CONTACTS_ERROR_NONE)
                            {
                                if (is_default == false)
                                {
                                	dlog_print(DLOG_INFO, LOG_TAG,"Many numbers were defined for this contact and this is not a default one. We ignore this number.");
                                }
                                else
                                {

                                    //Default phone number found
                                    err = contacts_record_get_str(child_record_phone, _contacts_number.number, &email_address);
                                    if (err == CONTACTS_ERROR_NONE)
                                    {
                                        // Successfully retrieved the phone number
                                        // so we set the bool to know that we don't need a placeholder.
                                        is_phone_number = true;
                                        break; //We have found a default number, we can finish browsing.
                                    }
                                    else
                                    {
                                    	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number! Error code: %d", err);
                                    }
                                }
                            }
                            else
                            {
                            	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when getting the 'is default' attribute! Error code: %d", err);
                            }
                        }
                        else
                        {
                        	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the phone number record! Error code: %d", err);
                        }
                    }
                }
                else
                {
                    // No phone numbers were found for the specified contact.
                    // This can happen for all contacts where no phone number was defined.
                    // You can do something specific to the no-phone contacts, for example filter them.
                    // In this snippet we have already set the bool is_phone_number to false by default,
                    // so we do nothing here.
                }
            }
            else
            {
            	dlog_print(DLOG_ERROR, LOG_TAG,"Error occurred when getting the count of phone numbers! Error code: %d", err);
            }
            if(is_phone_number == false)
            {
                // we set the placeholder in place of the phone number.
                email_address = strdup("Not defined");
            }

            // Here you can make use of the retrieved contact name and phone number.
            // In this snippet it's only printed as a log:
            dlog_print(DLOG_INFO, LOG_TAG,"Contact name: %s,\t Email address: %s", contact_name, email_address);

            //strcpy(emails[counter], email_address);
            //strncpy(emails[counter], email_address,sizeof(emails[counter]));
            //sprintf(emails[counter],"%s",email_address);
            strcpy(emails_t[counter], email_address);

            counter++;

            free(contact_name);
            free(email_address);

            contacts_list_next(contacts_list); // proceed to next contact on a list
        }
        dlog_print(DLOG_INFO, LOG_TAG,"Listing contacts completed!");

        contacts_list_destroy(contacts_list, true);
    }
    else
    {
    	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when getting the contact list! Error code: %d", err);
    }

    if (contacts_disconnect() != CONTACTS_ERROR_NONE)
    {
    	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when disconnecting from contacts service!");
    }
}
else
{
	dlog_print(DLOG_ERROR, LOG_TAG, "Error occurred when connecting to contacts service! Error code: %d", err);
}

	return counter;
}

void run_contacts(int *email_count, int *number_count){

	*number_count = contacts_initialization_number();
	*email_count = contacts_initialization_email();
}
