/*
 * protibadi-data-processing.c
 *
 *  Created on: Jul 15, 2016
 *      Author: User
 */
#include <app.h>
#include <dlog.h>

#include "main.h"
#include "protibadi-data-processing.h"
#include "protibadi-sensor.h"
#include "protibadi-email.h"
#include "protibadi-messages.h"
#include "protibadi-contacts.h"

static double latitude, longitude;

static int email_count = 0;
static int number_count = 0;

void run_app() {
	run_location(&latitude,&longitude);
	dlog_print(DLOG_DEBUG, LOG_TAG, "Lat: %lf, Long: %lf", latitude, longitude);

	run_contacts(&email_count, &number_count);

	int i;

	for(i=0;i<email_count;i++) {
		emails[i] = emails_t[i];
		dlog_print(DLOG_DEBUG, LOG_TAG, "Converted data type (email): %s", emails[i]);
	}

	for(i=0;i<number_count;i++) {
		numbers[i] = numbers_t[i];
		dlog_print(DLOG_DEBUG, LOG_TAG, "Converted data type (number): %s", numbers[i]);
	}

	run_email(emails, email_count, latitude,longitude);

	run_sms(numbers, number_count, latitude, longitude);

	/*free(emails);
	free(numbers);
	free(emails_t);
	free(numbers_t);*/

	sms_close_service();
}
