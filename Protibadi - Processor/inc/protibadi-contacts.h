/*
 * protibadi-contacts.h
 *
 *  Created on: Aug 4, 2016
 *      Author: mansib
 */

#ifndef PROTIBADI_CONTACTS_H_
#define PROTIBADI_CONTACTS_H_

#include "main.h"

void run_contacts(int *email_count, int *number_count);


#endif /* PROTIBADI_CONTACTS_H_ */
