/*
 * protibadi-email.h
 *
 *  Created on: Aug 3, 2016
 *      Author: Mansib
 */

#ifndef PROTIBADI_EMAIL_H_
#define PROTIBADI_EMAIL_H_

#include "main.h"

void run_email(char *emails[], int email_count, double latitude, double longitude);


#endif /* PROTIBADI_EMAIL_H_ */
