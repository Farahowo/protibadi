#ifndef __MAIN_H__
#define __MAIN_H__

#include <Elementary.h>
#include <app.h>
#include <dlog.h>
#include <efl_extension.h>
#include <telephony.h>
#include <system_settings.h> //Mansib
#include <locations.h> //Mansib
#include <email.h>
#include <messages.h>
#include <contacts.h>

#define ELM_DEMO_EDJ "/opt/usr/apps/org.example.protibadi/res/uicomponents.edj" //Mansib

#define _PRINT_MSG_LOG_BUFFER_SIZE_ 1024
#define PRINT_MSG(fmt, args...) do { char _log_[_PRINT_MSG_LOG_BUFFER_SIZE_]; \
    snprintf(_log_, _PRINT_MSG_LOG_BUFFER_SIZE_, fmt, ##args); _add_entry_text(_log_); } while (0)

typedef struct {
    Evas_Object *win;
    Evas_Object *navi;
    Evas_Object *conform;
} appdata_s;

void _add_entry_text(const char *text);
void _new_button(appdata_s *ad, Evas_Object *display, char *name, void *cb);
Evas_Object *_create_new_cd_display(appdata_s *ad, char *name, void *cb);
Eina_Bool _pop_cb(void *data, Elm_Object_Item *item);

#ifndef PACKAGE
#define PACKAGE "org.example.protibadi"
#endif

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "protibadi"

#endif                           /* __MAIN_H__ */
