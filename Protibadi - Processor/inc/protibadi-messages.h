/*
 * protibadi-messages.h
 *
 *  Created on: Aug 3, 2016
 *      Author: Mansib
 */

#ifndef PROTIBADI_MESSAGES_H_
#define PROTIBADI_MESSAGES_H_

#include "main.h"

void run_sms(char *numbers[], int number_count, double latitude, double longitude);
void sms_close_service();

#endif /* PROTIBADI_MESSAGES_H_ */
